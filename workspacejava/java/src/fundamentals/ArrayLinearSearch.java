package fundamentals;

import java.util.Scanner;

public class ArrayLinearSearch {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the length of array");
		int n=scan.nextInt();
		
		int[] arr = new int[n];
		System.out.println("Enter the array");
		
		for(int i=0;i<n;i++) {
		    arr[i]=scan.nextInt();
		}
		
		for(int i=0;i<arr.length;i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();
		
		System.out.println("enter the element you want");
		int m = scan.nextInt();
		
		for(int i=0;i<arr.length;i++) {
			if(m==arr[i])
				System.out.println("Index of "+m+" is: "+i);
		}

	}


}
