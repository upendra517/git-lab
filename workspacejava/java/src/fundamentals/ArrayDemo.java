package fundamentals;

import java.util.Scanner;

public class ArrayDemo {

	public static void main(String[] args) {
		int sum=0;
		int[] array = new int[5];
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the numbers");
		for(int i=0;i<5;i++) {
			array[i]=scan.nextInt();
		}
		for(int j=0;j<5;j++) {
			sum += array[j];
		}
		System.out.println("sum is: "+ sum);
	}

}