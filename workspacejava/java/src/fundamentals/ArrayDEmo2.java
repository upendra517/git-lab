package fundamentals;

import java.util.Scanner;

public class ArrayDEmo2 {

	public static void main(String[] args) {
		int[] array = new int[5];
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the numbers");
		for(int i=0;i<5;i++) {
			array[i]=scan.nextInt();
		}
     int minimum = findMinimum(array);
     System.out.println("minimum number is: "+minimum);
	}
	public static int findMinimum(int a[]) {
		int min = a[0];
		
		for(int j=0;j<a.length;j++) {
			if(a[j]<min) {
				min = a[j];
			}
		}
		return min;
	}

}
