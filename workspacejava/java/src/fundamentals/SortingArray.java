package fundamentals;

import java.util.Scanner;

public class SortingArray {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the elements");
		int arr[]= new int[5];
		 for(int i=0;i<arr.length;i++) {
			 arr[i]=scan.nextInt();
			 }
		 
		 System.out.print("Before sorting array: ");
		 for(int j=0;j<arr.length;j++) {
			 System.out.print(arr[j]+" ");
		 }
		 System.out.println("");
		 
		int i,j;
		for(i=0;i<arr.length;i++) {
			for(j=i+1;j<arr.length;j++) {
				if(arr[i]>arr[j]) {
					int temp=arr[j];
					arr[j]=arr[i];
					arr[i]=temp;
				}
			}
		}
		
		System.out.print("After sorting array: ");
		for(int k=0;k<arr.length;k++) {
			System.out.print(arr[k]+" ");
		}
		System.out.println(" ");
	}

}

